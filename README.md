# MESSAGING - RabbitMQ Task

## Prerequisites:
Download and install RabbitMQ or execute the docker-compose.yml provided in this repository.
### Running docker compose
Go to the root folder of this repository and execute the following command:
```bash
docker-compose up
```
## Practical Task 1:
Implement a Spring Boot application for sending notifications to customers about the receipt of goods based on the following architecture:
![Task 1 Architecture](task1.png "Task 1 architecture")

### Notes
Failed Message Exchange is not configured as DLX for the source queues.Consumer is responsible to re-publish failed messages.

## Practical Task 2:
Update previous implementation and change retry mechanism from inprocess to retry exchange/queue. Retry queue should have ttl, after message expires it should be routed to the source queue.

![Task 2 Architecture](task2.png "Task 2 architecture")

### Notes
Retry exchange is not configured as DLX for the source queues. Consumer is responsible to re-publish messages for retry. If retry limit reached message should be re-published to Failed Message Exchange instead.

## Practical Task 3:
Update previous implementation, configure message ttl, max size and dlx attributes on consumer queues. Expired messages or during queue overflow messages should be sent to DLQ by broker.

![Task 3 Architecture](task3.png "Task 3 architecture")

### Notes
Dead Letter exchange should be specified as DLX attribute on source queues in addition to message TTL
and max length attributes

## How to validate the tasks
In order to validate the tasks you must change the value the **app.task.active** property for one of the following values: **task1, task2 or task3**

You can change the application.yml directly:
```yml
app:
  task:
    active: task1
```

### Checking failed messages
To check the result for each task you can access this url http://localhost:8080/failed_messages

This is an example of response:
```json
[
    {
        "id": "9882cc3b-ba21-4234-8a22-c21834053d02",
        "notification": {
            "id": "78123ce3-c178-4df7-a32c-80a0e906eb1e",
            "message": "New good to warehouse 10",
            "status": "ERROR"
        },
        "reason": "DISCARDED"
    },
    {
        "id": "6326dcc3-9a70-4872-bbd6-2124406fa40f",
        "notification": {
            "id": "67e349f5-7c70-483d-8c65-ef725c809a4b",
            "message": "New good to warehouse 6",
            "status": "ERROR"
        },
        "reason": "RETURNED"
    }
]
```
Items with status **DISCARDED** means that the notification could not be processed after retries (task 1 and task 2) and items with status **RETURNED** means that was dead lettered by Rabbit MQ (task 3)

**NOTE:** Sometimes you need to run more than one time to simulate the result for task3 because RabbitMQ is processing the message so fast that is not able to generate the scenario where the queue is full.

#### Creating new notification messages
Some messages is create during application startup in order to be easy to check the task. If you want to create more messages you can use this endpoint: http://localhost:8080/notificaton

This in an example of payload with valid message that will be processed:
```json
{
  "message": "New notification",
  "status:": "NEW"
}
```

This in an example of payload with invalid message that will be generate an error:
```json
{
  "message": "New notification with error",
  "status:": "ERROR"
}