package com.epam.message.rabbitmq.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.epam.message.rabbitmq.common.NotificationConsumer;
import com.epam.message.rabbitmq.common.ReceiptNotification;

@ConditionalOnProperty(
    value = "app.task.active",
    havingValue = "task1"
)
@Component
public class NotificationConsumerTask1 implements NotificationConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationConsumerTask1.class);
    public static final Long MAX_RETRIES = 3L;
    public static final String RETRY_PROP = "n-retries";

    private final RabbitTemplate rabbitTemplate;

    public NotificationConsumerTask1(final RabbitTemplate template) {
        rabbitTemplate = template;
    }

    @RabbitListener(queues = { Config.QUEUE_MESSAGES })
    public void consume(Message<ReceiptNotification> in) {
        LOGGER.info("Processing message: {}", in.getPayload());
        var retries = in.getHeaders().get(RETRY_PROP, Long.class);
        if (retries != null && retries.equals(MAX_RETRIES)) {
            sendToFailedQueue(in);
            return;
        }

        if ("ERROR".equals(in.getPayload().status())) {
            var numberOfRetries = retries == null ? 1 : retries + 1;
            sendToRetry(in, numberOfRetries);
            return;
        }
        
        LOGGER.info("Notification message processed!");
    }

    private void sendToRetry(final Message<ReceiptNotification> in, final long numberOfRetries) {
        this.rabbitTemplate.convertAndSend(
            Config.EXCHANGE_MESSAGES, 
            Config.QUEUE_MESSAGES, 
            in.getPayload(), 
            m ->  { 
                m.getMessageProperties().getHeaders().put(RETRY_PROP, numberOfRetries); 
                return m;
            }
        );
    }
   
    private void sendToFailedQueue(Message<ReceiptNotification> in) {
        LOGGER.info("Max retries reached: {}", MAX_RETRIES);
        this.rabbitTemplate.convertAndSend(
            Config.FAILED_EXCHANGE_MESSAGES,
            Config.FAILED_QUEUE_MESSAGES,
            in.getPayload()
        );
    }
    
}
