package com.epam.message.rabbitmq.task1;

import java.util.stream.IntStream;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.message.rabbitmq.common.ReceiptNotification;

@ConditionalOnProperty(
    value = "app.task.active",
    havingValue = "task1"
)
@Configuration
public class Config {

    public static final String QUEUE_MESSAGES = "task1-queue";
    public static final String EXCHANGE_MESSAGES = "task1-exchange";
    
    public static final String FAILED_QUEUE_MESSAGES = "failed-task1-queue";
    public static final String FAILED_EXCHANGE_MESSAGES = "failed-task1-exchange";

    @Bean
    public ApplicationRunner runner(RabbitTemplate rabbitTemplate) {
        return args -> {
            IntStream.range(0, 2).forEach(i -> {
                rabbitTemplate.convertAndSend(
                        Config.EXCHANGE_MESSAGES,
                        Config.QUEUE_MESSAGES,
                        ReceiptNotification.of("New notification receipt %d".formatted(i + 1), "NEW"));
                rabbitTemplate.convertAndSend(
                        Config.EXCHANGE_MESSAGES,
                        Config.QUEUE_MESSAGES,
                        ReceiptNotification.of("New notification receipt with error %d".formatted(i), "ERROR"));
            });
        };
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    Queue messagesQueue() {
        return QueueBuilder.durable(QUEUE_MESSAGES)
                .build();
    }

    @Bean
    TopicExchange messagesExchange() {
        return new TopicExchange(EXCHANGE_MESSAGES);
    }
    
    @Bean
    Binding bindingMessages() {
        return BindingBuilder.bind(messagesQueue()).to(messagesExchange())
                .with(QUEUE_MESSAGES);
    }

    @Bean
    Queue failedQueue() {
        return QueueBuilder.durable(FAILED_QUEUE_MESSAGES)
                .build();
    }

    @Bean
    DirectExchange failedMessagesExchange() {
        return new DirectExchange(FAILED_EXCHANGE_MESSAGES);
    }

    @Bean
    Binding failedBindingMessages() {
        return BindingBuilder.bind(failedQueue()).to(failedMessagesExchange())
                .with(FAILED_QUEUE_MESSAGES);
    }
}
