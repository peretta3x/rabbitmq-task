package com.epam.message.rabbitmq.task3;

import java.util.stream.IntStream;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.message.rabbitmq.common.ReceiptNotification;

@ConditionalOnProperty(
    value = "app.task.active",
    havingValue = "task3"
)
@Configuration
public class Config {

    public static final String QUEUE_MESSAGES = "task3-queue";
    public static final String EXCHANGE_MESSAGES = "task3-exchange";

    public static final String RETRY_QUEUE_MESSAGES = "retry-task3-queue";
    public static final String RETRY_EXCHANGE = "retry3-exchange";
    
    public static final String FAILED_QUEUE_MESSAGES = "failed-task3-queue";
    public static final String FAILED_EXCHANGE_MESSAGES = "failed-task3-exchange";

    public static final String DLQ_QUEUE_MESSAGES = "dlq-task3-queue";
    public static final String DLQ_EXCHANGE_MESSAGES = "dlq-task3-exchange";
    
    @Bean
    public ApplicationRunner runner(RabbitTemplate rabbitTemplate) {
        return args -> {
            IntStream.range(0, 10).forEach(i -> {
                rabbitTemplate.convertAndSend(
                        Config.EXCHANGE_MESSAGES,
                        Config.QUEUE_MESSAGES,
                        ReceiptNotification.of("New notification receipt %d".formatted(i + 1), "NEW"));
                rabbitTemplate.convertAndSend(
                        Config.EXCHANGE_MESSAGES,
                        Config.QUEUE_MESSAGES,
                        ReceiptNotification.of("New notification receipt with error %d".formatted(i + 1), "ERROR"));
            });
        };
    }

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        final var rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(producerJackson2MessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    Queue autoRetryQueue() {
        return QueueBuilder.durable(RETRY_QUEUE_MESSAGES)
                .withArgument("x-message-ttl", 6000)
                .withArgument("x-dead-letter-exchange", EXCHANGE_MESSAGES)
                .withArgument("x-dead-letter-routing-key", QUEUE_MESSAGES)
                .build();
    }

    @Bean
    Exchange autoRetryExchange() {
        return ExchangeBuilder.topicExchange(RETRY_EXCHANGE).build();
    }

    @Bean
    Binding bindingRetry(Queue autoRetryQueue, TopicExchange autoRetryExchange) {
        return BindingBuilder.bind(autoRetryQueue).to(autoRetryExchange).with(RETRY_QUEUE_MESSAGES);
    }

    @Bean
    Queue messagesQueue() {
        return QueueBuilder.durable(QUEUE_MESSAGES)
                .withArgument("x-max-length", 3)
                .withArgument("x-overflow", "reject-publish-dlx")
                .withArgument("x-dead-letter-exchange", DLQ_EXCHANGE_MESSAGES)
                .withArgument("x-dead-letter-routing-key", DLQ_QUEUE_MESSAGES)
                .build();
    }

    @Bean
    Queue dlqQueue() {
        return QueueBuilder.durable(DLQ_QUEUE_MESSAGES)
            .build();
    }

    @Bean
    Queue failedQueue() {
        return QueueBuilder.durable(FAILED_QUEUE_MESSAGES)
                .build();
    }

    @Bean
    TopicExchange messagesExchange() {
        return new TopicExchange(EXCHANGE_MESSAGES);
    }

    @Bean
    TopicExchange dlqMessagesExchange() {
        return ExchangeBuilder.topicExchange(DLQ_EXCHANGE_MESSAGES).build();
    }

    @Bean
    DirectExchange failedMessagesExchange() {
        return new DirectExchange(FAILED_EXCHANGE_MESSAGES);
    }

    @Bean
    Binding bindingMessages() {
        return BindingBuilder.bind(messagesQueue()).to(messagesExchange())
                .with(QUEUE_MESSAGES);
    }

    @Bean
    Binding bindingDlqMessages() {
        return BindingBuilder.bind(dlqQueue()).to(dlqMessagesExchange()).with(DLQ_QUEUE_MESSAGES);
    }

    @Bean
    Binding failedBindingMessages() {
        return BindingBuilder.bind(failedQueue()).to(failedMessagesExchange())
                .with(FAILED_QUEUE_MESSAGES);
    }
}
