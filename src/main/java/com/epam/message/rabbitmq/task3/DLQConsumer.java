package com.epam.message.rabbitmq.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.epam.message.rabbitmq.common.FailedNotification;
import com.epam.message.rabbitmq.common.FailedNotificationRespository;
import com.epam.message.rabbitmq.common.ReceiptNotification;

@ConditionalOnProperty(
    value = "app.task.active",
    havingValue = "task3"
)
@Component
public class DLQConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DLQConsumer.class);

    private final FailedNotificationRespository messageStore;

    public DLQConsumer(FailedNotificationRespository messageStore) {
        this.messageStore = messageStore;
    }

    @RabbitListener(queues = { Config.DLQ_QUEUE_MESSAGES })
    void consume(Message<ReceiptNotification> message) {
        LOGGER.info("DLQ message: {}", message.getPayload());
        LOGGER.info("DLQ message headers: {}", message.getHeaders());
        messageStore.persist(FailedNotification.of(message.getPayload(), "RETURNED"));
    }
    
}
