package com.epam.message.rabbitmq.task3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.epam.message.rabbitmq.common.FailedNotification;
import com.epam.message.rabbitmq.common.FailedNotificationRespository;
import com.epam.message.rabbitmq.common.ReceiptNotification;

@ConditionalOnProperty(
    value = "app.task.active",
    havingValue = "task3"
)
@Component
public class FailedMessagesConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(FailedMessagesConsumer.class);
    private final FailedNotificationRespository messageStore;

    public FailedMessagesConsumer(final FailedNotificationRespository messageStore) {
        this.messageStore = messageStore;
    }

    @RabbitListener(queues = { Config.FAILED_QUEUE_MESSAGES })
    public void consume(final Message<ReceiptNotification> message) {
        final var receiptNotification = message.getPayload();
        LOGGER.info("Saving failed message in storage: {}", receiptNotification);
        messageStore.persist(FailedNotification.of(message.getPayload(), "DISCARDED"));
    }
    
}
