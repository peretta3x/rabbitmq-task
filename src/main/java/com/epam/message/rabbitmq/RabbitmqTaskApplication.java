package com.epam.message.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RabbitmqTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitmqTaskApplication.class, args);
	}

}
