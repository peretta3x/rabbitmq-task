package com.epam.message.rabbitmq.task2;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.epam.message.rabbitmq.common.NotificationConsumer;
import com.epam.message.rabbitmq.common.ReceiptNotification;

@ConditionalOnProperty(
    value = "app.task.active",
    havingValue = "task2"
)
@Component
public class NotificationConsumerTask2 implements NotificationConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationConsumerTask2.class);
    public static final Long MAX_RETRIES = 3L;
    private final RabbitTemplate rabbitTemplate;

    public NotificationConsumerTask2(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @RabbitListener(queues = { Config.QUEUE_MESSAGES })
    public void consume(Message<ReceiptNotification> in) {
        LOGGER.info("Processing message: {}", in.getPayload());
        final var death = getDeathCount(in);
        if (death != null && death.equals(MAX_RETRIES)) {
            sendToFailedQueue(in);
            return;
        }

        if ("ERROR".equals(in.getPayload().status())) {
            sendToRetry(in);
            return;
        }
        
        LOGGER.info("Notification message processed!");
    }

    private void sendToRetry(final Message<ReceiptNotification> in) {
        rabbitTemplate.convertAndSend(
            Config.RETRY_EXCHANGE,
            Config.RETRY_QUEUE_MESSAGES,
            in.getPayload(),
            m ->  { 
                m.getMessageProperties().getHeaders().putAll(in.getHeaders());
                return m;
            }
        );
    }

   
    private void sendToFailedQueue(Message<ReceiptNotification> in) {
        LOGGER.info("Max retries reached: {}", MAX_RETRIES);
        this.rabbitTemplate.convertAndSend(
            Config.FAILED_EXCHANGE_MESSAGES,
            Config.FAILED_QUEUE_MESSAGES,
            in.getPayload()
        );
    }

    private Long getDeathCount(final Message<ReceiptNotification> in) {
        var deathHeader = in.getHeaders().get("x-death", List.class);
        Map<String, Object> death = deathHeader != null  && deathHeader.size() > 0
            ? (Map<String, Object>) deathHeader.get(0)
            : null;
        return death == null ? 0L : (Long) death.get("count");
    }
}
