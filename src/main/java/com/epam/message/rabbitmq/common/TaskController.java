package com.epam.message.rabbitmq.common;

import java.util.List;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class TaskController {
    
    private final FailedNotificationRespository messageStore;
    private final RabbitTemplate rabbitTemplate;
    
    @Value("${app.task.active}-exchange")
    private String exchangeName;

    @Value("${app.task.active}-queue")
    private String queueName;

    public TaskController(final FailedNotificationRespository messageStore, final RabbitTemplate rabbitTemplate) {
        this.messageStore = messageStore;
        this.rabbitTemplate = rabbitTemplate;
    }

    @GetMapping("/failed_messages")
    public List<FailedNotification> failed() {
        return messageStore.getAll();
    }

    @PostMapping("/notification")
    public void createNewNotification(@RequestBody ReceiptNotification receiptNotification) {
        System.out.println(exchangeName);
        rabbitTemplate.convertAndSend(
                    exchangeName,
                    queueName,
                    receiptNotification);
    }
}
