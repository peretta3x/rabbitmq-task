package com.epam.message.rabbitmq.common;

import java.io.Serializable;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

public record ReceiptNotification(
    @JsonProperty("id") String id,
    @JsonProperty("message") String message,
    @JsonProperty("status") String status
) implements Serializable {

    public static ReceiptNotification of(final String message, final String status) {
        return new ReceiptNotification(UUID.randomUUID().toString(), message, status);
    }
}
