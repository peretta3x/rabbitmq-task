package com.epam.message.rabbitmq.common;

import java.util.UUID;

public record FailedNotification(
    String id,
    ReceiptNotification notification,
    String reason
) {
    public static FailedNotification of(final ReceiptNotification notification, final String reason) {
        return new FailedNotification(UUID.randomUUID().toString(), notification, reason);
    }
}
