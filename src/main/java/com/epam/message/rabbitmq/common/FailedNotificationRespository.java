package com.epam.message.rabbitmq.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class FailedNotificationRespository {
    
    private Map<String, FailedNotification> store = new HashMap<>();

    public void persist(final FailedNotification failedNotification) {
        store.put(failedNotification.id(), failedNotification);
    }

    public List<FailedNotification> getAll() {
        return new ArrayList<>(store.values());
    }

}


