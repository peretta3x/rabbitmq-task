package com.epam.message.rabbitmq.common;

import org.springframework.messaging.Message;

public interface NotificationConsumer {

    void consume(Message<ReceiptNotification> message);

}
